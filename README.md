# home-assistant-stable

This repo/registry will contain the last release of the next-to-newest version of home assistant.  

Ie, for the 2022.9 version a number of releases will be made.  
As soon as the 2022.10 version is released, the last 2022.9 release is considered stable and will be pulled into this registry. Once a 2022.11 release is made the last 2022.10 will be pulled.
This way early bugs will not be present when using this image and it should be much more stable for automatic update usage.
