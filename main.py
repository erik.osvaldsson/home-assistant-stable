debug_prints = False

if debug_prints:
    print("Main entry")

import json
import os
import re
import math

import natsort
import requests

# Read API
payload = []
if os.path.isfile("dump.json"):
    with open("dump.json") as f:
        payload = json.load(f)
else:
    if debug_prints:
        print(f"No local dump found, http request")

    items_to_get = 1000
    max_per_page = 100
    for i in range(0, math.ceil(items_to_get / max_per_page)):
        r = requests.get(
            "https://hub.docker.com/v2/repositories/homeassistant/home-assistant/tags",
            params={"page": f"{i+1}", "page_size": f"{max_per_page}"},
        )
        p = r.json()
        for p2 in p["results"]:
            payload.append(p2)

        if debug_prints:
            print(f'Request {i+1} gave {len(p["results"])} with code {r.status_code}')

    with open("dump.json", "w") as f:
        json.dump(payload, f)

if debug_prints:
    print("Read dump data")

# Find all with proper pattern
# re_tester = re.compile("^\d{4}\.\d{1,2}\.\d+$")
re_tester = re.compile("^\d{4}\.\d{1,2}$")
tags_to_consider = []
if debug_prints:
    print(f"Found {len(payload)} tags")
for t in payload:
    tag = t["name"]
    result = re_tester.match(tag)
    if result:
        tags_to_consider.append(t)

# Some debug printing
for t in payload:
    tag = t["name"]
    # print(tag)

# Get the next-to-newest tag name
tag_names = [x["name"] for x in tags_to_consider]
tag_names = natsort.natsorted(
    tag_names, reverse=True
)  # Use natsort as numbers in strings are tricky to sort
if debug_prints:
    print("Matched tags:")
    for tag in tag_names:
        print(f"\t{tag}")
if len(tag_names) < 2:
    # too few relevant tags found
    if debug_prints:
        print(f"Only {len(tag_names)} tags found")
    exit(1)
wanted_tag = tag_names[1]

print(f"{wanted_tag}")
